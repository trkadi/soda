import { browser, element, by } from 'protractor';

export class SzoprobaPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('szp-app h1')).getText();
  }
}
