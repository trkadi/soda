import { SzoprobaPage } from './app.po';

describe('szoproba App', () => {
  let page: SzoprobaPage;

  beforeEach(() => {
    page = new SzoprobaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('szp works!');
  });
});
