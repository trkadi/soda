import { Injectable } from '@angular/core';
import { IList } from '../models/interfaces/list.interface';
import { IWord } from '../models/interfaces/word.interface';

@Injectable()
export class IDBService {

  private appDB: IDBDatabase;
  private opCache: any[];

  constructor() {
    this.opCache = [];

    // Create IDB, setup callbacks
    const appDBOpenDBRequest = indexedDB.open('AppData_old');

    appDBOpenDBRequest.addEventListener('error', (evt: any) => { // FIXME: Use correct type!
      // The open request failed..
      console.error(`Database error: ${evt.target.errorCode}`);
    });

    appDBOpenDBRequest.addEventListener('success', (evt: any) => { // FIXME: Use correct type!
      // The open was successful, no new db version!
      this.appDB = evt.target.result;

      // Note: only after this point is this.appDB available, here we have to call all pre-cached operations..
      this.opCache.forEach(operation => operation());

      // Then get rid of this mess..
      delete this.opCache;
    });

    appDBOpenDBRequest.addEventListener('upgradeneeded', (evt: any) => { // FIXME: Use correct type!
      // The database is firstly created or version number increased.
      this.appDB = evt.target.result;

      this.appDB.createObjectStore('lists', {
        keyPath: 'uuid',
      });

      this.appDB.createObjectStore('words', {
        keyPath: 'uuid',
      });
    });
  }

  loadLists(callback: Function) {
    if (!this.appDB) {
      this.opCache.push(this.loadLists.bind(this, callback));
      return;
    }

    // FIXME: Typescript don't recognise IDBObjectStore.getAll ...
    (this.appDB.transaction('lists', 'readonly').objectStore('lists') as any).getAll().addEventListener(
      'success',
      (evt: any) => { // FIXME: Use correct type!
        callback(evt.target.result);
      }
    );
  }

  loadWords(callback: Function) {
    if (!this.appDB) {
      this.opCache.push(this.loadWords.bind(this, callback));
      return;
    }

    // FIXME: Typescript don't recognise IDBObjectStore.getAll ...
    (this.appDB.transaction('words', 'readonly').objectStore('words') as any).getAll().addEventListener(
      'success',
      (evt: any) => { // FIXME: Use correct type!
        callback(evt.target.result);
      }
    );
  }

  saveLists(lists: IList[]) {
    if (!this.appDB) {
      this.opCache.push(this.saveLists.bind(this, lists));
      return;
    }

    const listObjectStore = this.appDB.transaction('lists', 'readwrite').objectStore('lists');

    // Remove removed lists
    // FIXME: Typescript don't recognise IDBObjectStore.getAll ...
    (listObjectStore as any).getAll().addEventListener(
      'success',
      (evt: any) => { // FIXME: Use correct type!

        // Update and remove
        // NOTE: for..of is more efficient than .forEach()
        for (const dbListItem of (evt.target.result as IList[])) {
          const foundReduxListItem = lists.find(reduxListItem => reduxListItem.uuid === dbListItem.uuid);

          if (!foundReduxListItem) {
            // Remove from DB!
            listObjectStore.delete(dbListItem.uuid);
          } else if (foundReduxListItem && foundReduxListItem.modificationDate > dbListItem.modificationDate) {
            // Update only if needed!
            listObjectStore.put(foundReduxListItem);
          }
        }

        // Add new items
        for (const reduxListItem of lists) {
          if (!evt.target.result.find((dbListItem: IList) => dbListItem.uuid === reduxListItem.uuid)) {
            listObjectStore.add(reduxListItem);
          }
        }

      }
    );
  }

  saveWords(words: IWord[]) {
    if (!this.appDB) {
      this.opCache.push(this.saveWords.bind(this, words));
      return;
    }

    const wordObjectStore = this.appDB.transaction('words', 'readwrite').objectStore('words');

    // Remove removed words
    // FIXME: Typescript don't recognise IDBObjectStore.getAll ...
    (wordObjectStore as any).getAll().addEventListener(
      'success',
      (evt: any) => { // FIXME: Use correct type!

        // Update and remove
        // NOTE: for..of is more efficient than .forEach()
        for (const dbWordItem of (evt.target.result as IWord[])) {
          const foundReduxWordItem = words.find(reduxWordItem => reduxWordItem.uuid === dbWordItem.uuid);

          if (!foundReduxWordItem) {
            // Remove from DB!
            wordObjectStore.delete(dbWordItem.uuid);
          } else if (foundReduxWordItem && foundReduxWordItem.modificationDate > dbWordItem.modificationDate) {
            // Update only if needed!
            wordObjectStore.put(foundReduxWordItem);
          }
        }

        // Add new items
        for (const reduxWordItem of words) {
          if (!evt.target.result.find((dbWordItem: IWord) => dbWordItem.uuid === reduxWordItem.uuid)) {
            wordObjectStore.add(reduxWordItem);
          }
        }

      }
    );
  }
}
