// FIXME: Refactor this whole mess: provide it only in word-test component.. state handling better..

// Core imports
import { Injectable } from '@angular/core';

// Interface imports
import { IList } from '../models/interfaces/list.interface';
import { IWord } from '../models/interfaces/word.interface';
import { ICounter } from '../models/interfaces/counter.interface';
import { ISettings } from '../models/interfaces/settings.interface';

// // Reactive things
import { BehaviorSubject ,  Observable ,  Subscription } from 'rxjs';
import { select } from '@angular-redux/store';

@Injectable()
export class DebriefService {

  word$: BehaviorSubject<IWord>;
  counter$: BehaviorSubject<ICounter>;

  @select(['settings']) settings$: Observable<ISettings>;
  @select(['words']) words$: Observable<IWord[]>;
  @select(['lists']) lists$: Observable<IList[]>;

  private word: IWord;
  private counter: ICounter;
  private subscriptions: Subscription[];
  private originalWordList: IWord[];
  private wordList: IWord[];
  private currentList: IList;
  private settings: ISettings;
  private lists: IList[];
  private words: IWord[];

  constructor() {

    this.originalWordList = null;
    this.wordList = null;
    this.subscriptions = [];
    this.word = undefined;
    this.counter = {
      all: 0,
      asked: 0,
      correct: 0
    };

    this.word$ = new BehaviorSubject<IWord>(this.word);
    this.counter$ = new BehaviorSubject<ICounter>(this.counter);

    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        this.settings = settings;

        this.updateData();
      }),
      this.lists$.subscribe(lists => {
        if (!lists) {
          return;
        }

        this.lists = lists;

        this.updateData();
      }),
      this.words$.subscribe(words => {
        if (!words) {
          return;
        }

        this.words = words;

        this.updateData();
      })
    );
  }

  restart() {
    if (!this.originalWordList) {
       return;
    }

    this.wordList = [];

    if (this.settings.listMode === 'marked') {
      this.originalWordList.forEach(word => {
        if (word.marked) {
          this.wordList.push(word);
        }
      });
    } else {
      this.originalWordList.forEach(word => {
        this.wordList.push(word);
      });
    }

    this.counter = {
      all: this.wordList.length,
      asked: 0,
      correct: 0
    };

    this.counter$.next(this.counter);

    this.rollWord();
  }

  checkWord(enteredWord: string) {
    if (enteredWord.toLowerCase() !== this.word.meaning.toLowerCase()) {
      return;
    }

    this.counter.correct++;
    this.counter$.next(this.counter);
    this.rollWord();
  }

  rollWord() {
    if (this.wordList.length < 1) {
      this.word = null;
      this.word$.next(this.word);

      this.word = undefined;
      return;
    }

    const randomIndex = this.getRandomIndex(0, this.wordList.length - 1);

    this.word = this.wordList.splice(randomIndex, 1)[0];
    this.word$.next(this.word);

    this.counter.asked++;
    this.counter$.next(this.counter);
  }

  private getRandomIndex(min: number, max: number) {
    return Math.round((max - min) * Math.random()) + min;
  }

  private updateData() {
    if (!this.settings.list || !this.lists || !this.words) {
      return;
    }

    this.currentList = this.lists.find(list => list.uuid === this.settings.list);

    if (this.currentList) {
      this.originalWordList = this.words.filter(word => word.listUUID === this.currentList.uuid);
    } else {
      this.originalWordList = null;
    }

    this.restart();
  }
}
