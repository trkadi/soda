export interface ISettings {
  lang?: string;
  theme?: string;
  list?: string;
  listMode?: string;
  difficulty?: string;
}
