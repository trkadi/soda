export interface IWord {
  uuid: string;
  listUUID: string;
  creationDate: Date;
  modificationDate: Date;
  value: string;
  meaning: string;
  notes?: string;
  marked?: boolean;
}
