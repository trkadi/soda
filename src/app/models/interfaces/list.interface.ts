import { IWord } from './word.interface';

export interface IList {
  uuid: string;
  name: string;
  description: string;
  creationDate: Date;
  modificationDate: Date;
}
