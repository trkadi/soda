export interface ICounter {
  all: number;
  asked: number;
  correct: number;
}
