// Core, Angular related
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { environment } from '../environments/environment';

// Locales
import { registerLocaleData } from '@angular/common';
import localeHU from '@angular/common/locales/hu';
registerLocaleData(localeHU);

// Reactive things
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';
import { SettingsActions } from './redux/actions/settings.actions';
import { WordListActions } from './redux/actions/word-list.actions';
import { rootReducer } from './redux/reducers/root.reducer';

// Pipes
import { ByteFormat } from './pipes/byte-format.pipe';

// Components
import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { WordTestComponent } from './components/word-test/word-test.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ResultsComponent } from './components/results/results.component';
import { MyListsComponent } from './components/my-lists/my-lists.component';
import { EditListComponent } from './components/edit-list/edit-list.component';
import { AddListComponent } from './components/add-list/add-list.component';
import { AddWordComponent } from './components/add-word/add-word.component';

const reduxLogger = require('redux-logger');

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    MainMenuComponent,
    WordTestComponent,
    SettingsComponent,
    ResultsComponent,
    MyListsComponent,
    ByteFormat,
    EditListComponent,
    AddListComponent,
    AddWordComponent
  ],
  imports: [
    BrowserModule,
    NgReduxModule,
    RouterModule.forRoot(appRoutes, {
      useHash: true
    })
  ],
  providers: [
    SettingsActions,
    WordListActions,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private ngRedux: NgRedux<any>,
    private devTools: DevToolsExtension
  ) {

    let enhancers = [];
    let middlewares = [];

    if (!environment.production) {
      middlewares = [ ...middlewares, reduxLogger.createLogger() ];

      if (devTools.isEnabled()) {
        enhancers = [ ...enhancers, devTools.enhancer() ];
      }
    }

    ngRedux.configureStore(rootReducer, {}, middlewares, enhancers);
  }

}
