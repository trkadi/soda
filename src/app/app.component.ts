// Core imports
import { Component, OnInit, OnDestroy } from '@angular/core';

// Reactive things
import { select } from '@angular-redux/store';
import { Subscription ,  Observable } from 'rxjs';
import { SettingsActions } from './redux/actions/settings.actions';
import { WordListActions } from './redux/actions/word-list.actions';

// Interfaces
import { ISettings } from './models/interfaces/settings.interface';
import { IList } from './models/interfaces/list.interface';
import { IWord } from './models/interfaces/word.interface';

// Language map
import { languageMap } from './language-map';

// IDB service
import { IDBService } from './services/idb.service';
import { DebriefService } from './services/debrief.service';

// // Mimic
// import 'mimic';

@Component({
  selector: 'szp-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [IDBService, DebriefService]
})
export class AppComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[];
  private lists: IList[];
  private settings: ISettings;

  @select(['settings']) settings$: Observable<ISettings>;
  @select(['lists']) lists$: Observable<IList[]>;
  @select(['words']) words$: Observable<IWord[]>;

  constructor(
    private settingsActions: SettingsActions,
    private wordListActions: WordListActions,
    private iDBService: IDBService,
  ) {
    this.subscriptions = new Array<Subscription>();
    this.lists = new Array<IList>();
  }

  ngOnInit() {
    // ----- Load settings
    try {
      this.settingsActions.setSettingDispatch(JSON.parse(localStorage.settings));
    } catch (err) {
      console.warn(`Couldn't read saved SETTINGS (maybe because there weren't), falling back to default settings.`);
    }

    // ----- Load lists
    this.iDBService.loadLists(function(lists: IList[]) {
      this.wordListActions.addWordListsDispatch(lists);
    }.bind(this));

    // ----- Load words
    this.iDBService.loadWords(function(words: IWord[]) {
      this.wordListActions.addWordsDispatch(words);
    }.bind(this));

    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        // FIXME: Don't touch the dom like this. Is there something better maybe for the theme handling??
        document.body.dataset.theme = settings.theme;
        this.settings = settings;
        localStorage.setItem('settings', JSON.stringify(settings));
      }),
      this.lists$.subscribe(lists => {
        if (lists === null) {
          return;
        }

        this.lists = lists;

        if (this.settings.list && lists.length) {
          // Scenario #1: There are lists and the user also picked one.
          const listElement = lists.find(list => list.uuid === this.settings.list);

          if (listElement) {
            // Scenario #1/a: The picked ID is really valid.
            this.settingsActions.setSettingDispatch({
              list: listElement.uuid
            });
          } else {
            // Scenario #1/b: The picked ID is invalid.
            this.settingsActions.setSettingDispatch({
              list: lists[0].uuid
            });
          }
        } else if (!this.settings.list && lists.length) {
          // Scenario #2: There are lists but the user didn't pick yet (options are missing?).
          this.settingsActions.setSettingDispatch({
            list: lists[0].uuid
          });
        } else {
          // Scenario #3: There aren't lists and the user didn't pick..
          this.settingsActions.setSettingDispatch({
            list: undefined
          });
        }

        this.iDBService.saveLists(this.lists);
      }),
      this.words$.subscribe(words => {
        if (words === null) {
          return;
        }

        this.iDBService.saveWords(words);
      })
    );

  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

}
