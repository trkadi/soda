import { Routes } from '@angular/router';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { WordTestComponent } from './components/word-test/word-test.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ResultsComponent } from './components/results/results.component';
import { MyListsComponent } from './components/my-lists/my-lists.component';
import { EditListComponent } from './components/edit-list/edit-list.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: MainMenuComponent
  }, {
    path: 'word-test',
    component: WordTestComponent
  }, {
    path: 'my-lists',
    component: MyListsComponent
  }, {
    path: 'edit-list/:uuid',
    component: EditListComponent
  }, {
    path: 'settings',
    component: SettingsComponent,
  }, {
    path: 'result',
    component: ResultsComponent
  }
];
