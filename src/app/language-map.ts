// TODO: Különféle route-okhoz külön külön fájlban legyenek a fordítások a lehető legegyszerűbb alakban (key - value).,

export const languageMap = {
  'en-GB': {
    'displayName': 'English',
    'mainMenu': {
      'startPractice': 'Practice words',
      'myLists': 'My lists',
      'settings': 'Settings',
      'help': 'Help'
    },
    'settings': {
      'screenTitle': 'Settings',
      'wordListMode': {
        'label': 'Which part should we practice?',
        'options': {
          'complete': 'Complete list',
          'marked': 'Marked words',
        }
      },
      'difficulty': {
        'label': 'How fast should it be? (s)',
        'options': {
          'timeless': 'Without time',
          'easy': 'Easy (12)',
          'medium': 'Medium (8)',
          'hard': 'Hard (4)',
          'insane': 'Insane! (2)',
        }
      },
      'themeListLabel': 'Appearance',
      'languageListLabel': 'Program language',
      'wordListLabel': 'Which list of yours should we practice?',
    },
    'wordTest': {
      'inputPlaceholder': 'Type or use speech recognition!'
    },
    'myLists': {
      'screenTitle': 'My lists',
      'emptyState': `You don't have lists yet!`,
      'creationDate': 'Created on',
      'modificationDate': 'Modified on',
      'containedWords': 'Word pairs',
      'marked': 'marked',
      'listIsEmpty': 'Empty',
      'description': 'Description'
    },
    'addList': {
      'title': {
        'add': 'Add new list',
        'edit': 'Edit list'
      },
      'nameTip': 'What should be it\'s name?',
      'descriptionTip': 'Write a few words for yourself about it!',
      'addButton': {
        'add': 'Add',
        'edit': 'Save'
      }
    },
    'editList': {
      'invalidListId': 'Invalid list ID',
      'emptyState': `You don't have words on this list yet!`,
      'notes': 'Notes',
      'missingNotes': `There aren't notes`
    },
    'addWord': {
      'title': {
        'add': 'Add new word',
        'edit': 'Edit word'
      },
      'valueTip': `What is the foreign word?`,
      'meaningTip': `Type here the meaning!`,
      'notesTip': 'Write a memo for yourself!',
      'addButton': {
        'add': 'Add',
        'edit': 'Save'
      }
    }
  },
  'hu-HU': {
    'displayName': 'Magyar',
    'mainMenu': {
      'startPractice': 'Szavak gyakorlása',
      'myLists': 'Listáim',
      'settings': 'Beállítások',
      'help': 'Segítség'
    },
    'settings': {
      'screenTitle': 'Beállítások',
      'wordListMode': {
        'label': 'Melyik részét gyakoroljuk?',
        'options': {
          'complete': 'Egészet',
          'marked': 'Megjelölt szavakat',
        }
      },
      'difficulty': {
        'label': 'Milyen gyors legyen? (mp)',
        'options': {
          'timeless': 'Idő nélküli',
          'easy': 'Könnyű (12)',
          'medium': 'Közepes (8)',
          'hard': 'Nehéz (4)',
          'insane': 'Őrült! (2)',
        }
      },
      'themeListLabel': 'Megjelenés',
      'languageListLabel': 'Program nyelve',
      'wordListLabel': 'Melyik listádat gyakoroljuk?',
    },
    'wordTest': {
      'inputPlaceholder': 'Írj vagy használj beszédfelismerést!'
    },
    'myLists': {
      'screenTitle': 'Listáim',
      'emptyState': `Nincsenek még listáid!`,
      'creationDate': 'Létrehozva',
      'modificationDate': 'Módosítva',
      'containedWords': 'Szópárok',
      'marked': 'megjelölt',
      'listIsEmpty': 'Üres',
      'description': 'Leírás'
    },
    'addList': {
      'title': {
        'add': 'Új lista hozzáadása',
        'edit': 'Lista szerkesztése'
      },
      'nameTip': 'Mi legyen a neve?',
      'descriptionTip': 'Írj róla pár szót magadnak!',
      'addButton': {
        'add': 'Hozzáadás',
        'edit': 'Mentés'
      }
    },
    'editList': {
      'invalidListId': 'Érvénytelen lista ID',
      'emptyState': 'Nincsenek még szavak ezen a listán!',
      'notes': 'Jegyzetek',
      'missingNotes': 'Nincsenek jegyzetek'
    },
    'addWord': {
      'title': {
        'add': 'Új szó hozzáadása',
        'edit': 'Szó szerkesztése'
      },
      'valueTip': 'Mi az idegen szó?',
      'meaningTip': 'Írd ide a jelentését!',
      'notesTip': 'Írj ide magadnak széljegyzetet róla!',
      'addButton': {
        'add': 'Hozzáadás',
        'edit': 'Mentés'
      }
    }
  }
};
