import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'byteformat'
})
export class ByteFormat implements PipeTransform {
  transform(bytes: number, args: number[]): string {
    return this.formatBytes(bytes, args ? args[0] : undefined);
  }

  private formatBytes(bytesToConvert, decimalPlace?) {
    if (0 == bytesToConvert) {
      return '0 Bytes';
    }

    const conversionNumber = 1024;
    const decimals = decimalPlace || 2;
    const list = [ 'Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ];
    const f = Math.floor( Math.log(bytesToConvert) / Math.log(conversionNumber) );

    return parseFloat((bytesToConvert / Math.pow(conversionNumber, f)).toFixed(decimals)) + ' ' + list[f]}
}





