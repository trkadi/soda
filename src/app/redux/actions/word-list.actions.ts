import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';

@Injectable()
export class WordListActions {

  static ADD_LISTS = 'ADD_LISTS';
  static DELETE_LISTS = 'DELETE_LISTS';
  static EDIT_LISTS = 'EDIT_LISTS';

  static ADD_WORDS = 'ADD_WORDS';
  static DELETE_WORDS = 'DELETE_WORDS';
  static EDIT_WORDS = 'EDIT_WORDS';
  // static MARK_WORDS = 'MARK_WORDS';
  // static UNMARK_WORDS = 'UNMARK_WORDS';
  static TOGGLE_MARK_WORDS = 'TOGGLE_MARK_WORDS';

  constructor(private ngRedux: NgRedux<any>) {}

  /** ----- ADD_LISTS ----- */
  addWordListsDispatch(wordLists: IList[]) {
    this.ngRedux.dispatch(this.addWordLists(wordLists));
  }

  private addWordLists(wordLists: IList[]) {
    return {
      type: WordListActions.ADD_LISTS,
      payload: wordLists
    };
  }

  /** ----- DELETE_LISTS ----- */
  deleteWordListsDispatch(uuids: string[]) {
    this.ngRedux.dispatch(this.deleteWordLists(uuids));
  }

  private deleteWordLists(uuids: string[]) {
    return {
      type: WordListActions.DELETE_LISTS,
      payload: uuids
    };
  }

  /** ----- EDIT_LISTS ----- */
  editWordListsDispatch(wordLists: IList[]) {
    this.ngRedux.dispatch(this.editWordLists(wordLists));
  }

  private editWordLists(wordLists: IList[]) {
    return {
      type: WordListActions.EDIT_LISTS,
      payload: wordLists
    };
  }

  /** ----- ADD_WORDS ----- */
  addWordsDispatch(words: IWord[]) {
    this.ngRedux.dispatch(this.addWords(words));
  }

  private addWords(words: IWord[]) {
    return {
      type: WordListActions.ADD_WORDS,
      payload: words
    };
  }

  /** ----- DELETE_WORDS ----- */
  deleteWordsDispatch(uuids: string[]) {
    this.ngRedux.dispatch(this.deleteWords(uuids));
  }

  private deleteWords(uuids: string[]) {
    return {
      type: WordListActions.DELETE_WORDS,
      payload: uuids
    };
  }

  /** ----- EDIT_WORDS ----- */
  editWordsDispatch(words: IWord[]) {
    this.ngRedux.dispatch(this.editWords(words));
  }

  private editWords(words: IWord[]) {
    return {
      type: WordListActions.EDIT_WORDS,
      payload: words
    };
  }

  // /** ----- MARK_WORDS ----- */
  // markWordsDispatch(uuids: string[]) {
  //   this.ngRedux.dispatch(this.markWords(uuids));
  // }

  // private markWords(uuids: string[]) {
  //   return {
  //     type: WordListActions.MARK_WORDS,
  //     payload: uuids
  //   };
  // }

  // /** ----- UNMARK_WORDS ----- */
  // unmarkWordsDispatch(uuids: string[]) {
  //   this.ngRedux.dispatch(this.unmarkWords(uuids));
  // }

  // private unmarkWords(uuids: string[]) {
  //   return {
  //     type: WordListActions.UNMARK_WORDS,
  //     payload: uuids
  //   };
  // }

  /** ----- TOGGLE_MARK_WORDS ----- */
  toggleMarkWordsDispatch(options: { uuids: string[], modificationDate: Date}) {
    this.ngRedux.dispatch(this.toggleMarkWords(options));
  }

  private toggleMarkWords(options: { uuids: string[], modificationDate: Date}) {
    return {
      type: WordListActions.TOGGLE_MARK_WORDS,
      payload: options
    };
  }
}
