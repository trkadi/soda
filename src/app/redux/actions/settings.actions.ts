import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { ISettings } from '../../models/interfaces/settings.interface';

@Injectable()
export class SettingsActions {

  static SET_SETTING = 'SET_SETTING';

  constructor(private ngRedux: NgRedux<any>) {}


  /** ----- SET_OPTION ----- */
  setSettingDispatch(settings: ISettings) {
    this.ngRedux.dispatch(this.setSettings(settings));
  }

  private setSettings(settings: ISettings) {
    return {
      type: SettingsActions.SET_SETTING,
      payload: settings
    };
  }

  /** ----- X ----- */
}
