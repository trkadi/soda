import { SettingsActions } from '../actions/settings.actions';
import { ISettings } from '../../models/interfaces/settings.interface';

const defaultState = (): ISettings => {
  return {
    lang: 'en-GB',
    theme: 'default',
    listMode: 'complete',
    list: undefined,
    difficulty: 'timeless'
  };
};

export function SettingsReducer(state = defaultState(), action: any) {
  switch (action.type) {
    /** ----- SettingsActions.SET_SETTING */
    case SettingsActions.SET_SETTING:
      return Object.assign({}, state, action.payload);

    /** ----- DEFAULT */
    default:
      return state;
  }
}
