import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IWord } from '../../models/interfaces/word.interface';


export function WordReducer(state: IWord[] = null, action: any) {
  switch (action.type) {
    /** ----- WordListActions.ADD_WORDS */
    case WordListActions.ADD_WORDS: {
      let newState;

      if (state === null) {
        newState = [];
      } else {
        newState = Array.prototype.slice.call(state);
      }

      newState.push(...action.payload);

      // Reverse sort by date
      newState.sort((a, b) => {
        if (a.creationDate < b.creationDate) {
          return 1;
        } else if (a.creationDate > b.creationDate ) {
          return -1;
        }

        return 0;
      });

      return newState;
    }

    /** ----- WordListActions.EDIT_WORDS */
    case WordListActions.EDIT_WORDS: {
      if (state === null) {
        return [];
      } else {
        return state.map(stateWord => {
          const matchingWord = action.payload.find(payloadWord => payloadWord.uuid === stateWord.uuid);

          if (matchingWord) {
            return matchingWord;
          }

          return stateWord;
        });
      }
    }

    /** ----- WordListActions.DELETE_WORDS */
    case WordListActions.DELETE_WORDS: {
      if (state === null) {
        return state;
      }

      return state.filter(stateWord  => action.payload.indexOf(stateWord.uuid) < 0);
    }

    /** ----- WordListActions.TOGGLE_MARK_WORDS */
    case WordListActions.TOGGLE_MARK_WORDS: {
      if (state === null) {
        return state;
      }

      return state.map(stateWord => {
        const matchingUUID = action.payload.uuids.find(payloadUUID => payloadUUID === stateWord.uuid);

        if (matchingUUID) {
          stateWord.marked = !stateWord.marked;
          stateWord.modificationDate = action.payload.modificationDate;
        }

        return stateWord;
      });
    }

    /** ----- DEFAULT */
    default:
      return state;
  }
}
