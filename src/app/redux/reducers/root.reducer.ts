import { combineReducers } from 'redux';

import { SettingsReducer } from './settings.reducer';
import { ListReducer } from './list.reducer';
import { WordReducer } from './word.reducer';

export const rootReducer = combineReducers<{}>({
  'settings': SettingsReducer,
  'lists': ListReducer,
  'words': WordReducer,
});
