import { IList } from '../../models/interfaces/list.interface';
import { WordListActions } from '../../redux/actions/word-list.actions';

export function ListReducer(state = null, action: any) {
  switch (action.type) {
    /** ----- WordListActions.ADD_LISTS */
    case WordListActions.ADD_LISTS: {
      let newState;

      if (state === null) {
        newState = [];
      } else {
        newState = Array.prototype.slice.call(state);
      }

      newState.push(...action.payload);

      // Reverse sort by date
      newState.sort((a, b) => {
        if (a.creationDate < b.creationDate) {
          return 1;
        } else if (a.creationDate > b.creationDate ) {
          return -1;
        }

        return 0;
      });

      return newState;
    }

    /** ----- WordListActions.EDIT_LISTS */
    case WordListActions.EDIT_LISTS: {
      if (state === null) {
        return [];
      } else {
        return state.map(stateList => {
          const matchingList = action.payload.find(payloadList => payloadList.uuid === stateList.uuid);

          if (matchingList) {
            return matchingList;
          }

          return stateList;
        });
      }
    }

    /** ----- WordListActions.DELETE_LISTS */
    case WordListActions.DELETE_LISTS: {
      if (state === null) {
        return state;
      }

      return state.filter(wordList  => action.payload.indexOf(wordList.uuid) < 0);
    }

    /** ----- DEFAULT */
    default:
      return state;
  }
}
