// Core imports
import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild,
  HostListener,
} from '@angular/core';

// Language
import { languageMap } from '../../language-map';

// Reactive things
import { Observable ,  Subscription } from 'rxjs';
import { select } from '@angular-redux/store';
import { ISettings } from '../../models/interfaces/settings.interface';
import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';

@Component({
  selector: 'szp-my-lists',
  templateUrl: './my-lists.component.html',
  styleUrls: ['./my-lists.component.scss']
})
export class MyListsComponent implements OnInit, OnDestroy {

  languageMap: Object;
  settings: ISettings;
  modalOpened: boolean;
  editingUUID: string;
  wordCounts: Object;

  @select(['lists']) lists$: Observable<IList[]>;
  @select(['words']) words$: Observable<IWord[]>;
  @select(['settings']) settings$: Observable<ISettings>;

  @ViewChild('addListDialog') addListDialog;

  private subscriptions: Subscription[];
  private words: IWord[];

  @HostListener('window:keydown', ['$event'])
    keyDownHandler(evt: KeyboardEvent) {
      if (evt.key === 'Enter' && !this.modalOpened) {
        this.openModal();
      }
    }

  constructor(
    private componentRootElement: ElementRef,
    private wordListActions: WordListActions
  ) {
    this.languageMap = languageMap;
    this.subscriptions = new Array<Subscription>();
    this.modalOpened = false;
    this.editingUUID = '';
    this.wordCounts = {};
  }

  ngOnInit() {
    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        this.settings = settings;
        this.componentRootElement.nativeElement.dataset.theme = settings.theme;
      }),
      this.words$.subscribe(words => {
        if (!words) {
          return;
        }

        this.words = words;

        // TODO: Check if it's fast enough. If not somehow I need to store this.
        this.wordCounts = {};
        for (const word of words) {
          if (!this.wordCounts[word.listUUID]) {
            this.wordCounts[word.listUUID] = [1, 0];
          } else {
            this.wordCounts[word.listUUID][0]++;
          }

          if (word.marked) {
            this.wordCounts[word.listUUID][1]++;
          }
        }
      })
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  openModal(uuid?: string) {
    this.modalOpened = true;
    this.editingUUID = uuid;
  }

  closeModal() {
    this.modalOpened = false;
    this.editingUUID = '';
  }

  deleteList(uuid: string) {
    // FIXME: [IMPORTANT] Other languages..
    if (confirm('Are you sure about this? Your words in this list will be lost [FOREVER].')) {
      this.wordListActions.deleteWordListsDispatch([uuid]);

      // TODO: Be able to warn the user to do something with the words..
      this.wordListActions.deleteWordsDispatch(this.words.filter(word => word.listUUID === uuid).map(word => word.uuid));
    }
  }

}
