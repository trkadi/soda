// FIXME: What if I reload on a list with no words?

import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { Router } from '@angular/router';
import { DebriefService } from '../../services/debrief.service';

// Reactive things
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';
import { ISettings } from '../../models/interfaces/settings.interface';

// Languagemap
import { languageMap } from '../../language-map';

@Component({
  selector: 'szp-word-test',
  templateUrl: './word-test.component.html',
  styleUrls: ['./word-test.component.scss'],
})
export class WordTestComponent implements OnInit, OnDestroy {

  languageMap: Object;
  currentList: IList;
  isInputEnabled: Boolean;
  displayedWord: IWord;
  settings: ISettings;

  @ViewChild('answerinput') answerInput: ElementRef;

  @select(['settings']) settings$: Observable<ISettings>;
  @select(['lists']) lists$: Observable<IList[]>;
  // @HostBinding('class.overlayed') isOverlayActive: boolean;

  private subscriptions: Subscription[];
  private answerCheckTimeout: NodeJS.Timer;
  private emptyWord: IWord;

  constructor(
    private dataService: DebriefService,
    private router: Router,
    private wordListActions: WordListActions
  ) {
    this.languageMap = languageMap;
    this.subscriptions = [];
    this.isInputEnabled = false;
    this.emptyWord = {
        uuid: undefined,
        listUUID: undefined,
        creationDate: null,
        modificationDate: null,
        value: '-',
        meaning: undefined,
        notes: '-',
    };

    this.displayedWord = this.emptyWord;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        if (!settings.list) {
          this.router.navigateByUrl('/');
        }

        this.settings = settings;
      }),
      this.lists$.subscribe(lists => {
        if (!lists) {
          return;
        }

        this.currentList = lists.find(list => list.uuid === this.settings.list);
      }),
      this.dataService.word$.subscribe(word => {
        if (!word) {
          if (word === null) {
            if (confirm('Do you want to restart?')) {
              this.dataService.restart();
            } else {
              this.dataService.restart();
              this.router.navigateByUrl('/');
            }
          }

          return;
        }

        this.clearInput();

        if (!word) {
          this.isInputEnabled = false;
          this.displayedWord = this.emptyWord;
          return;
        }

        this.isInputEnabled = true;
        this.displayedWord = word;

        // Wait for enabling input
        setTimeout(() => this.answerInput.nativeElement.focus(), 0);
      })
    );

    this.dataService.restart();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  onAnswerInput(event: Event) {
    clearTimeout(this.answerCheckTimeout);

    const enteredValue = (event.currentTarget as HTMLInputElement).value.trim();

    if (!enteredValue) {
      return;
    }

    this.answerCheckTimeout = setTimeout(() => {
      this.dataService.checkWord(enteredValue);
    }, 500);
  }

  onKeyDownHandler(event: KeyboardEvent) {
    if (event.key === 'Enter' && event.shiftKey) {
      this.dataService.rollWord();
    }
  }

  // TODO: https://gitlab.com/trkadi/wordlearningapp/issues/3
  toggleMarkWord(displayedWord: IWord) {
    // if (displayedWord === this.emptyWord) {
    //   return;
    // }

    // this.wordListActions.toggleMarkWordsDispatch({
    //   uuids: [displayedWord.uuid],
    //   modificationDate: new Date()
    // });
  }

  private clearInput() {
    this.answerInput.nativeElement.value = '';
  }

}
