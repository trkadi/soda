// Core imports
import { Component, OnInit, OnDestroy } from '@angular/core';

// Reactive things
import { Subscription ,  Observable } from 'rxjs';

// Interfaces
import { DebriefService } from '../../services/debrief.service';
import { ICounter } from '../../models/interfaces/counter.interface';

@Component({
  selector: 'szp-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, OnDestroy {

  counter: ICounter;

  private subscriptions: Subscription[];

  constructor(
    private dataService: DebriefService
  ) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.dataService.counter$.subscribe(counter => this.counter = counter)
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

}
