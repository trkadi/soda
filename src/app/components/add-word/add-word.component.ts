// Core imports
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  HostListener,
  OnDestroy,
  ViewChild,
  ElementRef,
  Input,
} from '@angular/core';

const UUIDv1 = require('uuid/v1');

// Reactive things
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';

// Language map
import { languageMap } from '../../language-map';

@Component({
  selector: 'szp-add-word',
  templateUrl: './add-word.component.html',
  styleUrls: ['./add-word.component.scss'],
})
export class AddWordComponent implements OnInit, OnDestroy {

  languageMap: Object;
  lang: string;

  @Input() listUUID: string;
  @Input() editingUUID: string;

  @Output() close: EventEmitter<boolean>;

  @ViewChild('value') value: ElementRef;
  @ViewChild('meaning') meaning: ElementRef;
  @ViewChild('notes') notes: ElementRef;

  @select(['settings', 'lang']) lang$: Observable<string>;
  @select(['lists']) lists$: Observable<IList[]>;
  @select(['words']) words$: Observable<IWord[]>;

  private subscriptions: Subscription[];
  private listToEdit: IList;
  private words: IWord[];

  @HostListener('window:keydown', ['$event'])
    keydownHandler(evt: KeyboardEvent) {
      if (evt.key === 'Escape') {
        this.emitClose();
      }
    }

  constructor(
    private wordListActions: WordListActions
  ) {
    this.close = new EventEmitter();
    this.languageMap = languageMap;
    this.subscriptions = new Array<Subscription>();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.lang$.subscribe(lang => this.lang = lang),
      this.lists$.subscribe(lists => {
        this.listToEdit = lists.find(list => list.uuid === this.listUUID);
      }),
      this.words$.subscribe(words => {
        this.words = words.filter(word => word.listUUID === this.listUUID);
      })
    );

    if (this.editingUUID) {
      const matchedWord = this.words.find(word => this.editingUUID === word.uuid);

      this.value.nativeElement.value = matchedWord.value;
      this.meaning.nativeElement.value = matchedWord.meaning;
      this.notes.nativeElement.value = matchedWord.notes;
    }

    setTimeout(() => { this.value.nativeElement.focus(); }, 0);
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  emitClose() {
    this.close.emit();
  }

  addEdit() {
    const value = this.value.nativeElement.value.trim();
    const meaning = this.meaning.nativeElement.value.trim();
    const notes = this.notes.nativeElement.value.trim();

    if (!value || !meaning) {
      return;
    }

    /* ----- Edit word */
    if (this.editingUUID) {
      const wordToEdit = this.words.find(word => word.uuid === this.editingUUID);

      this.wordListActions.editWordsDispatch([{
        uuid: wordToEdit.uuid,
        listUUID: wordToEdit.listUUID,
        creationDate: wordToEdit.creationDate,
        modificationDate: new Date(),
        value: value,
        meaning: meaning,
        notes: notes,
        marked: wordToEdit.marked,
      }]);

      this.emitClose();

      return;
    }


    const existingWord = this.words.find(word => {
      if (word.value.toLowerCase() === value.toLowerCase() &&
          word.meaning.toLowerCase() === meaning.toLowerCase()) {
        return true;
      }
    });

    if (existingWord) {
      // FIXME: Change to multiple language..
      alert(`Why do you want to add the same word-meaning again?`);
      return;
    }

    const timestamp = new Date();

    this.wordListActions.addWordsDispatch([{
      uuid: UUIDv1(),
      listUUID: this.listUUID,
      creationDate: timestamp,
      modificationDate: timestamp,
      value: value,
      meaning: meaning,
      notes: notes,
      marked: false,
    }]);

    this.emitClose();
  }
}
