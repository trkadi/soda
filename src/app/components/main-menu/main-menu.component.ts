// Core imports
import { Component, OnInit, OnDestroy } from '@angular/core';

// Reactive things
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';

// Interfaces
import { ISettings } from '../../models/interfaces/settings.interface';
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';

// Languagemap
import { languageMap } from '../../language-map';

// FIXME: Typescript 2.5.2 does not contain this!
interface StorageEstimate {
  quota: number;
  usage: number;
}

@Component({
  selector: 'szp-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit, OnDestroy {

  languageMap: Object;
  isPracticeVisible: boolean;
  estimatedStorage: StorageEstimate;
  displayStorage: boolean;
  settings: ISettings;

  @select(['settings']) settings$: Observable<ISettings>;
  @select(['lists']) lists$: Observable<IList[]>;
  @select(['words']) words$: Observable<IWord[]>;

  private subscriptions: Subscription[];
  private lists: IList[];
  private words: IWord[];

  constructor() {
    this.languageMap = languageMap;
    this.subscriptions = [];
    this.isPracticeVisible = false;
    this.displayStorage = ((navigator as any).storage && (navigator as any).storage.estimate) ? true : false;
    this.estimatedStorage = {
      quota: 0,
      usage: 0
    };
  }

  ngOnInit() {
    // FIXME: Typescript 2.5.2 does not have this.
    if (this.displayStorage) {
      (navigator as any).storage.estimate().then(storageEstimation => this.estimatedStorage = storageEstimation);
    }

    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        this.settings = settings;

        this.isPracticable();
      }),
      this.lists$.subscribe(lists => {
        if (!lists) {
          return;
        }

        this.lists = lists;

        this.isPracticable();
      }),
      this.words$.subscribe(words => {
        if (!words) {
          return;
        }

        this.words = words;

        this.isPracticable();
      })
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  isPracticable() {
    if (!(this.settings && this.lists && this.words)) {
      return;
    }

    switch (this.settings.listMode) {
      /* ----- COMPLETE */
      case 'complete': {
        const matchingList = this.lists.find(list => list.uuid === this.settings.list);
        const matchingWords = this.words.find(word => word.listUUID === this.settings.list);

        this.isPracticeVisible = (this.settings.list && matchingList && matchingWords) ? true : false;
        break;
      }

      /* ----- MARKED */
      case 'marked': {
        const matchingList = this.lists.find(list => list.uuid === this.settings.list);
        const matchingWords = this.words.find(word => (word.listUUID === this.settings.list && word.marked));

        this.isPracticeVisible = (this.settings.list && matchingList && matchingWords) ? true : false;
        break;
      }

      /* ----- DEFAULT */
      default:
        this.isPracticeVisible = false;
        break;
    }
  }

}
