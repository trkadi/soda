// Core imports
import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef
} from '@angular/core';

// Interfaces
import { ISettings } from '../../models/interfaces/settings.interface';
import { IList } from '../../models/interfaces/list.interface';

// Reactive things
import { SettingsActions } from '../../redux/actions/settings.actions';
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';

// Languagemap
import { languageMap } from '../../language-map';

@Component({
  selector: 'szp-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  settings: ISettings;
  lists: IList[];
  activeListIndex: number;
  languageMap: Object;

  @select('settings') settings$: Observable<ISettings>;
  @select('lists') lists$: Observable<IList[]>;

  private languages: string[];
  private listModes: string[];
  private difficulties: string[];
  private subscriptions: Subscription[];

  constructor(
    private settingsActions: SettingsActions,
    private componentRootElement: ElementRef
  ) {
    this.activeListIndex = 0;
    this.languageMap = languageMap;
    this.subscriptions = new Array<Subscription>();
    this.languages = Object.keys(languageMap);
    this.lists = new Array<IList>();
    this.listModes = [
      'complete',
      'marked'
    ];
    this.difficulties = [
      'timeless',
      'easy',
      'medium',
      'hard',
      'insane',
    ];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.lists$.subscribe(lists => {
        this.lists = lists;
      }),
      this.settings$.subscribe(settings => {
        this.settings = settings;
        this.componentRootElement.nativeElement.dataset.theme = settings.theme;

        if (this.lists === null || !this.lists.length) {
          return;
        }

        this.lists.find((list, listIndex) => {
          this.activeListIndex = listIndex;
          return list.uuid === settings.list;
        });
      })
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  onDifficultyClick() {
    this.settingsActions.setSettingDispatch({
      difficulty: this.difficulties[this.getNextIndex(this.settings.difficulty, this.difficulties)]
    });
  }

  onWordListClick() {
    if (!this.lists.length) {
      return;
    }

    this.settingsActions.setSettingDispatch({
      list: this.lists[this.getNextIndex(
        this.lists.find(listElement => listElement.uuid === this.settings.list),
        this.lists
      )].uuid
    });
  }

  onWordListModeClick() {
    this.settingsActions.setSettingDispatch({
      listMode: this.listModes[this.getNextIndex(this.settings.listMode, this.listModes)]
    });
  }

  onThemeSelect(themeName: string) {
    this.settingsActions.setSettingDispatch({
      theme: themeName
    });
  }

  onLanguageChange() {
    this.settingsActions.setSettingDispatch({
      lang: this.languages[this.getNextIndex(this.settings.lang, this.languages)]
    });
  }

  private getNextIndex(arrayElement: any, array: any[]) {
    let index = array.indexOf(arrayElement);

    // If last element or couldn't find
    if (index > array.length - 2 || index === -1) {
      return 0;
    }

    return ++index;
  }
}
