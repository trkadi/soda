// Core imports
import {
  Component,
  OnInit,
  OnDestroy,
  HostListener,
  ElementRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Reactive things
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IList } from '../../models/interfaces/list.interface';
import { IWord } from '../../models/interfaces/word.interface';
import { ISettings } from '../../models/interfaces/settings.interface';

// Language map
import { languageMap } from '../../language-map';

@Component({
  selector: 'szp-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit, OnDestroy {
  activeList: IList;
  UUIDFromURL: string;
  settings: ISettings;
  languageMap: Object;
  modalOpened: boolean;
  words: IWord[];
  wordsOnThisList: IWord[];
  editingUUID: string;

  @select(['lists']) lists$: Observable<IList[]>;
  @select(['words']) words$: Observable<IWord[]>;
  @select(['settings']) settings$: Observable<ISettings>;

  private subscriptions: Subscription[];
  private lists: IList[];

  @HostListener('window:keydown', ['$event'])
    keyDownHandler(evt: KeyboardEvent) {
      if (evt.key === 'Enter' && !this.modalOpened) {
        this.openModal();
      }
    }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private wordListActions: WordListActions,
    private componentRootElement: ElementRef
  ) {
    this.subscriptions = new Array<Subscription>();
    this.languageMap = languageMap;
    this.modalOpened = false;
    this.editingUUID = '';
    this.words = []; // TODO: Use just array literal with every new Array() mess..
    this.wordsOnThisList = [];
  }

  ngOnInit() {
    this.subscriptions.push(
      this.settings$.subscribe(settings => {
        this.settings = settings;
        this.componentRootElement.nativeElement.dataset.theme = settings.theme;
      }),
      this.lists$.subscribe(lists => {
        this.lists = lists;

        this.setActiveList();
      }),
      this.activatedRoute.paramMap.subscribe(params => {
        this.UUIDFromURL = params.get('uuid');

        this.setActiveList();
      }),
      this.words$.subscribe(words => {
        if (!words) {
          return;
        }

        this.words = words;
        this.wordsOnThisList = words.filter(word => word.listUUID === this.UUIDFromURL);
      })
    );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  private setActiveList() {
    if (this.lists === null || !this.UUIDFromURL) {
      return;
    }

    this.activeList = this.lists.find(list => list.uuid === this.UUIDFromURL);

    if (!this.activeList) {
      this.router.navigateByUrl('/my-lists');
      return;
    }

    this.wordsOnThisList = this.words.filter(word => word.listUUID === this.UUIDFromURL);
  }

  openModal(uuid?: string) {
    this.modalOpened = true;
    this.editingUUID = uuid;
  }

  closeModal() {
    this.modalOpened = false;
    this.editingUUID = '';
  }

  deleteWord(uuid: string) {
    this.wordListActions.deleteWordsDispatch([uuid]);
  }

  toggleMarkWord(word: IWord) {
    this.wordListActions.toggleMarkWordsDispatch({
      uuids: [word.uuid],
      modificationDate: new Date()
    });
  }
}
