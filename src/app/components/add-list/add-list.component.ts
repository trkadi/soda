import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  HostListener,
  Input,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';

const UUIDv1 = require('uuid/v1');

// Reactive things
import { Subscription ,  Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { WordListActions } from '../../redux/actions/word-list.actions';

// Interfaces
import { IList } from '../../models/interfaces/list.interface';

// Language
import { languageMap } from '../../language-map';

@Component({
  selector: 'szp-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss']
})
export class AddListComponent implements OnInit, OnDestroy {

  languageMap: Object;
  lang: string;

  @Input() uuid: string;

  @Output() close: EventEmitter<boolean>;

  @ViewChild('listname') listName: ElementRef;
  @ViewChild('listdescription') listDescription: ElementRef;

  @select(['lists']) lists$: Observable<IList[]>;
  @select(['settings', 'lang']) lang$: Observable<string>;

  private subscriptions: Subscription[];
  private lists: IList[];

  @HostListener('window:keydown', ['$event'])
    keydownHandler(evt: KeyboardEvent) {
      if (evt.key === 'Escape') {
        this.emitClose();
      }
    }

  constructor(
    private wordListActions: WordListActions
  ) {
    this.close = new EventEmitter();
    this.subscriptions = new Array<Subscription>();
    this.languageMap = languageMap;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.lists$.subscribe(lists => {
        this.lists = lists;
      }),
      this.lang$.subscribe(lang => {
        this.lang = lang;
      })
    );

    if (this.uuid) {
      const matchedList = this.lists.find(list => this.uuid === list.uuid);

      this.listName.nativeElement.value = matchedList.name;
      this.listDescription.nativeElement.value = matchedList.description;
    }

    setTimeout(() => { this.listName.nativeElement.focus(); }, 0);
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) { subscription.unsubscribe(); }
  }

  emitClose() {
    this.close.emit();
  }

  addEdit() {
    const name = this.listName.nativeElement.value.trim();
    const description = this.listDescription.nativeElement.value.trim();

    if (!name || !description) {
      return;
    }

    // Add: már létezőt nem adhat hozzá

    /* ----- Edit list */
    if (this.uuid) {
      const listToEdit = this.lists.find(list => list.uuid === this.uuid);

      this.wordListActions.editWordListsDispatch([{
        uuid: this.uuid,
        name: name,
        description: description,
        creationDate: listToEdit.creationDate,
        modificationDate: new Date()
      }]);

      this.emitClose();

      return;
    }

    const existingList = this.lists.find(list => {
      if (list.name.toLowerCase() === name.toLowerCase() && list.description.toLowerCase() === description.toLowerCase()) {
        return true;
      }
    });

    if (existingList) {
      // TODO: If this is the case just display some message..
      return;
    }

    const timeStamp = new Date();

    this.wordListActions.addWordListsDispatch([{
      uuid: UUIDv1(),
      name: name,
      description: description,
      creationDate: timeStamp,
      modificationDate: timeStamp
    }]);

    this.emitClose();
  }
}
